﻿---
Title: Mário
Author: Miguel Caldas
---


“When your stepfather was dying in the hospital”

Said my mother

He would look at me and say:

“Save me! Save me!”

And I thought he was talking about the medication that caused him pain”

She took a sip from her glass and continued.


“But of course I was lying to myself.

He wasn’t talking about the medication…

And,

Yeah…

Not everyone dies well."

