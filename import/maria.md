﻿---
Title: Maria
Author: Miguel Caldas
---


As I touched my daughter's hands,

small, clean and uncreased,

I heard a voice:

 

"Life is the elegance of leaving."

 

I held her tight,

 

and thought of all the time I won't have with her.
