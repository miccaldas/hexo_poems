---
Title: Ice Storm
Author: Miguel Caldas
---

As everything expands to the end of its possibilities,

and the cold brings homeostasis and simplicity to the universe,

We bow,

gently,

at the unstopable violence of cause and effect.